# replicator

<div align="center">
  <img src="https://cobox.cloud/src/svg/logo.svg">
</div>

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

## Table of Contents

  - [About](#about)
  - [Install](#install)
  - [Usage](#usage)
  - [API](#api)
  - [Contributing](#contributing)
  - [License](#license)

## About
**CoBox** is an encrypted p2p file system and distributed back-up tool. [README](https://gitlab.com/coboxcoop/readme) provides a map of the project.

`replicator` provides a base class for replication. Initialises keys with validations, sets up storage paths, initialises feeds and enables replication.

These are all common functions for any space class, so we can inherit from the replicator class to extend functionality, such as for space, or admin space, which initialize additional but different logs.

## Install

```
npm i -g @coboxcoop/replicator

```

## Usage

```js
const Replicator = require('@coboxcoop/replicator')

// you can either create a replicator instance (see tests), or inherit to add further functionality, e.g.

class MyReplicatorClass extends Replicator {
  constructor (storage, address, identity, opts) {
    super(storage, address, identity, opts)

    // initFeeds must be called to initialize multifeed and storage correctly
    this._initFeeds({})
  }

  ready () {
    this.open(() => {
      // make your replicator ready, call any prep async functions
    })
  }
}
```

## API
See swagger documentation... (we won't have this for a while).

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

# License

[`AGPL-3.0-or-later`](./LICENSE)
