function removeEmpty (obj) {
  return Object.keys(obj)
    .filter(k => obj[k] != null)
    .reduce((newObj, k) => {
      return typeof obj[k] === "object"
        ? { ...newObj, [k]: removeEmpty(obj[k]) }
        : { ...newObj, [k]: obj[k] }
    }, {})
}

module.exports = { removeEmpty }
