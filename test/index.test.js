const { describe } = require('tape-plus')
const Corestore = require('corestore')
const crypto = require('@coboxcoop/crypto')
const encoder = require('@coboxcoop/crypto-encoder')
const fs = require('fs')
const path = require('path')
const randomWords = require('random-words')
const randomIP = require('random-ip')

const sinon = require('sinon')
const hyperswarm = require('hyperswarm')
const { encodings } = require('@coboxcoop/schemas')
const { Connection } = encodings.peer.connection

const { Replicator } = require('../')
const { replicate, tmp, cleanup } = require('./util')

function getIdentity () {
  const keyPair = crypto.boxKeyPair()
  return { publicKey: keyPair.publicKey, name: randomWords(1).pop() }
}

describe('basic', (context) => {
  context('valid', async (assert, next) => {
    const storage = tmp()
    const address = crypto.address()
    const replicator = new Replicator(storage, address, getIdentity())
    assert.ok(typeof replicator._initFeeds, 'returns a fn to init multifeeds')

    await replicator.ready()

    assert.ok(replicator.address, 'replicator created')
    assert.ok(Buffer.isBuffer(replicator.address), 'address is a buffer')
    assert.same(replicator.storage, path.join(storage, address.toString('hex')))
    assert.ok(replicator.feeds, 'defaults to initialize multifeed')
    assert.ok(replicator.identity.publicKey.length === 32, 'has a 32 byte publicKey')
    assert.ok(typeof replicator.replicate === 'function', 'has a replicate function')

    cleanup(storage, next)
  })

  context('invalid with bad address', async (assert, next) => {
    const storage = tmp()
    const address = Buffer.from('welcometothejungle')

    try {
      const replicator = new Replicator(storage, address, getIdentity())
    } catch (err) {
      assert.ok(err, 'no error')
      assert.same(err.message, 'Key is incorrect type', 'correct error message')
      assert.same(err.code, 'ERR_ASSERTION', 'throws ERR_ASSERTION')
    }

    cleanup(storage, next)
  })

  context('bytes used', async (assert, next) => {
    const storage = tmp()
    const address = crypto.address()

    const replicator = new Replicator(storage, address, getIdentity())

    await replicator.ready()

    assert.same(replicator.bytesUsed(), 0, 'defaults to 0')
    cleanup(storage, next)
  })

  context('replicate', (assert, next) => {
    const storage1 = tmp()
    const storage2 = tmp()
    const address = crypto.address()

    const replicator1 = new Replicator(storage1, address, getIdentity(), {
      corestore: new Corestore(storage1)
    })
    const replicator2 = new Replicator(storage2, address, getIdentity(), {
      corestore: new Corestore(storage2)
    })

    write(replicator1, 'dog', () => {
      write(replicator2, 'cat', () => {
        sync(check)
      })
    })

    function write (replicator, data, cb) {
      replicator.ready((err) => {
        assert.error(err, 'no error')
        replicator.feeds.writer('local', (err, feed) => {
          replicator.feed = feed
          assert.error(err, 'no error')
          feed.append(data, (err) => {
            assert.error(err, 'no error')
            cb()
          })
        })
      })
    }

    function sync (cb) {
      replicator1.ready(() => replicator2.ready(() => {
        replicate(replicator1, replicator2, () => {
          replicator1.ready(() => replicator2.ready(cb))
        })
      }))
    }

    function check () {
      replicator2.feeds.feed(replicator1.feed.key).get(0, (err, msg) => {
        assert.error(err, 'no error')
        assert.same(msg.toString(), 'dog', 'contains the replicated message')
        replicator1.feeds.feed(replicator2.feed.key).get(0, (err, msg) => {
          assert.error(err, 'no error')
          assert.same(msg.toString(), 'cat', 'contains the replicated message')
          cleanup([storage1, storage2], next)
        })
      })
    }
  })

  context('replicate with crypto encoder', (assert, next) => {
    const storage1 = tmp()
    const storage2 = tmp()
    const address = crypto.address()

    const replicator1 = new Replicator(storage1, address, {
      corestore: new Corestore(storage1)
    })

    const replicator2 = new Replicator(storage2, address, {
      corestore: new Corestore(storage2)
    })

    const valueEncoding = encoder(crypto.encryptionKey(), {
      nonce: encoder.generateNonce(),
      valueEncoding: 'binary'
    })

    replicator1._initFeeds({ valueEncoding })
    replicator2._initFeeds({ valueEncoding })

    write(replicator1, 'dog', () => {
      write(replicator2, 'cat', () => {
        sync(check)
      })
    })

    function write (replicator, data, cb) {
      replicator.ready((err) => {
        assert.error(err, 'no error')
        replicator.feeds.writer('local', (err, feed) => {
          replicator.feed = feed
          assert.error(err, 'no error')
          feed.append(data, (err, seq) => {
            assert.error(err, 'no error')
            cb()
          })
        })
      })
    }

    function sync (cb) {
      replicator1.ready(() => replicator2.ready(() => {
        replicate(replicator1, replicator2, (err) => {
          assert.error(err, 'no error')
          replicator1.ready(() => replicator2.ready(cb))
        })
      }))
    }

    function check () {
      replicator2.feeds.feed(replicator1.feed.key).get(0, (err, msg) => {
        assert.error(err, 'no error')
        assert.same(msg.toString(), 'dog', 'contains the replicated message')
        replicator1.feeds.feed(replicator2.feed.key).get(0, (err, msg) => {
          assert.error(err, 'no error')
          assert.same(msg.toString(), 'cat', 'contains the replicated message')
          cleanup([storage1, storage2], next)
        })
      })
    }
  })

  // context('swarm', async (assert, next) => {
  //   console.log('SWARM')
  //   const storage = tmp()
  //   const address = crypto.address()
  //   const swarm = hyperswarm()
  //   const stub = sinon.stub(swarm)

  //   const replicator = new Replicator(storage, address, { swarm: stub })

  //   assert.ok(typeof replicator.swarm === 'function', 'has a swarm function')
  //   assert.ok(typeof replicator.unswarm === 'function', 'has an unswarm function')

  //   await replicator.ready()

  //   assert.ok(replicator.discoveryKey, 'has a discovery key')

  //   replicator.swarm()
  //   // sinon.assert.calledWith(swarm.join, replicator.discoveryKey, { lookup: true, announce: true })

  //   replicator.unswarm()
  //   // sinon.assert.calledWith(swarm.leave, replicator.discoveryKey)

  //   replicator.hyperswarm.destroy()

  //   cleanup(storage, next)
  // })

  context('destroy replicator', async (assert, next) => {
    const storage = tmp()
    const address = crypto.address()
    const replicator = new Replicator(storage, address, getIdentity())
    assert.ok(typeof replicator._initFeeds, 'returns a fn to init multifeeds')

    await replicator.ready()

    assert.ok(replicator.address, 'replicator created')

    replicator.destroy(() => {
      assert.ok(!fs.existsSync(replicator.storage), 'storage destroyed')
      next()
    })
  })
})
