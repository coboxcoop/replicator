const Corestore = require('corestore')
const HypercoreDefaultStorage = require('hypercore-default-storage')
const Multifeed = require('@coboxcoop/multifeed')
const CoboxNetworker = require('@coboxcoop/networker')
const Nanoresource = require('nanoresource/emitter')
const assert = require('assert')
const crypto = require('@coboxcoop/crypto')
const debug = require('@coboxcoop/logger')('@coboxcoop/replicator')
const { encodings } = require('@coboxcoop/schemas')
const maybe = require('call-me-maybe')
const parallel = require('run-parallel')
const path = require('path')
const rimraf = require('rimraf')
const through = require('through2')

const { setupLevel, setupLiveStream } = require('./lib/level')
const defaultSettings = {
  threshold: 1, // number of seeders
  tolerance: 345600000 // 4 days in ms
}

const Connection = encodings.peer.connection

class Replicator extends Nanoresource {
  constructor (storage, address, identity, opts = {}) {
    super()

    if (opts.name) {
      this.name = opts.name
      delete opts.name
    }

    this._id = hex(crypto.randomBytes(2))
    this.address = crypto.toBuffer(address)
    this.repositoryId = hex(this.address)
    assert(crypto.isKey(this.address), 'invalid: address format')

    this.storage = path.join(storage, hex(this.address))
    this.identity = { publicKey: identity.publicKey, name: identity.name }

    this.settings = opts.settings || defaultSettings
    const store = opts.corestore || this.storage
    this.corestore = defaultCorestore(store, {
      ...opts,
      valueEncoding: 'binary'
    })

    this.network = opts.network || new CoboxNetworker(this.corestore)
    this.multinet = this.network.multinet

    this._opts = opts
    this.db = {}
    this.destroyed = false
    this._swarm = false
  }

  /**
   * @param {Replicator} replicator
   * @param {Object} {}
   */
  static async onCreate (replicator, opts = {}) {
    return true
  }

  ready (callback) {
    return maybe(callback, new Promise((resolve, reject) => {
      this.open((err) => {
        if (err) return reject(err)
        return resolve()
      })
    }))
  }

  destroy (callback) {
    return maybe(callback, new Promise((resolve, reject) => {
      this._preclose((err) => {
        if (err) return callback(err)
        this._destroyStorage((err) => {
          if (err) return callback(err)
          this.feeds.close((err) => {
            if (err) return reject(err)
            rimraf(this.storage, (err) => {
              if (err) return reject(err)
              resolve()
            })
          })
        })
      })
    }))
  }

  get attributes () {
    return {
      name: this.name,
      address: hex(this.address),
      settings: this.settings
    }
  }

  bytesUsed () {
    assert(this.feeds, 'multifeed not yet initialised, call _initFeeds')
    return this.feeds.feeds().map(f => f.byteLength).reduce((a, b) => a + b, 0)
  }

  isSwarming () {
    return this._swarm
  }

  async swarm (opts = { lookup: true, announce: true }) {
    debug({
      id: this._id,
      msg: 'opening connection',
      address: hex(this.address),
      discoveryKey: hex(this.discoveryKey)
    })

    // multinet.join generates the discoveryKey
    var val = await this.multinet.join(this.address, {
      live: true, mux: this.muxer, ...opts
    })

    this._swarm = true

    return val
  }

  async unswarm () {
    debug({
      id: this._id,
      msg: 'closing connection',
      address: hex(this.address),
      discoveryKey: hex(this.discoveryKey)
    })

    await this.multinet.leave(this.address)
    this._swarm = false

    return true
  }

  createLastSyncStream () {
    return this.lastSyncDb
      .createLiveStream()
      .pipe(through.obj(function (msg, _, next) {
        if (msg.sync) return next()
        next(null, {
          type: 'replicator/last-sync',
          data: {
            peerId: msg.key,
            address: this.repositoryId,
            lastSyncAt: msg.value
          }
        })
      }))
  }

  // -------------------------------------------------------------------------------------- //

  _open (callback) {
    if (this.destroyed) return callback(new Error('replicator: feeds destroyed'))
    if (!this.feeds) this._initFeeds()
    this.feeds.ready((err) => {
      if (err) return callback(err)
      this.discoveryKey = this.feeds.discoveryKey

      setupLevel(path.join(this.storage, 'db', 'connections'), { valueEncoding: Connection }, (err, db) => {
        if (err) return callback(err)
        this.db.connections = setupLiveStream(db)

        setupLevel(path.join(this.storage, 'lastSync'), { keyEncoding: 'utf-8', valueEncoding: 'json' }, (err, db) => {
          if (err) return callback(err)
          this.lastSyncDb = setupLiveStream(db)
          callback()
        })
      })
    })
  }

  _closeIndexes (cb) {
    return cb()
  }

  _close (callback = noop) {
    const self = this
    this.unswarm()
    onclose()

    function onclose () {
      self._preclose((err) => {
        if (err) return callback(err)
        self.feeds.close(callback)
      })
    }
  }

  _preclose (callback) {
    this._closeIndexes((err) => {
      if (err) return callback(err)
      this.db.connections.close((err) => {
        if (err) return callback(err)
        this.lastSyncDb.close(callback)
      })
    })
  }

  _destroyStorage (callback) {
    const tasks = []
    const persistenceFeed = this.feeds._handlers.feed
    tasks.push(initDestroy(persistenceFeed))

    // corestore still has these loaded in memory... so technically this isn't complete
    // however, corestore doesn't yet expose a destroy function
    Array
      .from(this.feeds._feedsByKey.values())
      .forEach((feed) => tasks.push(initDestroy(feed)))

    parallel(tasks, () => {
      this.destroyed = true
      return callback()
    })

    function initDestroy (destroyable) {
      return (done) => destroyable.destroyStorage(done)
    }
  }

  writer (cb) {
    return this.feeds.writer('local', this._coreOpts, cb)
  }

  feed (key) {
    return this.feeds.feed(key, this._coreOpts)
  }

  _initFeeds (opts = {}) {
    this._coreOpts = opts
    this.muxer = this.network.muxer(this.address, this._getFeed.bind(this))
    this.muxer.on('sync', this._storeSync.bind(this))
    this.feeds = new Multifeed(this.corestore, { ...opts, rootKey: this.address, muxer: this.muxer })
    this.replicate = this.feeds.replicate.bind(this.feeds)
  }

  _getFeed (key, cb) {
    const feed = this.corestore.get({ key, ...this._coreOpts })
    return cb(null, feed)
  }

  _storeSync (stream) {
    if (this.lastSyncDb.isClosed()) return debug(`_storeSync: lastSync for ${this.repositoryId} is closed`)
    const peerId = hex(stream.remotePublicKey)
    // storing a list of peers who share the space as keys, and their last seen time as values
    // dont wait, just put to the database
    this.lastSyncDb.put(peerId, Date.now())
  }
}

function noop () { }

function hex (buf) {
  if (Buffer.isBuffer(buf)) return buf.toString('hex')
  return buf
}

function isCorestore (storage) {
  return storage && storage.default && storage.get && storage.replicate && storage.close
}

function defaultCorestore (storage, opts) {
  if (isCorestore(storage)) return storage
  let factory
  if (typeof storage === 'function') {
    factory = (path) => storage(path)
  } else if (typeof storage === 'string') {
    factory = (path) => HypercoreDefaultStorage(storage + '/' + path)
  }
  return new Corestore(factory, opts)
}

module.exports = (storage, address, identity, opts) => new Replicator(storage, address, identity, opts)
module.exports.Replicator = Replicator
